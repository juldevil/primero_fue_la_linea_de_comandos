#!/bin/bash


clear


for funcion in sin cos tan
do

echo
echo "Crear el directorio $funcion"
echo
mkdir $funcion
echo
echo "Entrar al directorio $fucion"
echo
cd $funcion

cat > trigonometricas.py <<EOF
import math


for i in range(-10, 11):

    result = math.$funcion(i)

    with open('$funcion.txt', 'a') as f:
        f.write('{iteracion} {result}\n'.format(iteracion=i, result=result))
EOF

python trigonometricas.py

cd ..

done


exit 0
