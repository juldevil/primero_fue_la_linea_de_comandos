#!/bin/bash


clear


if [ ! -d prueba ];
then
    echo "El directorio no existe, será creado"
    echo
    mkdir prueba
else
    echo "El directorio ya existe"
    echo
fi


echo "Entrando al directorio"
echo
cd prueba


if [ -f archivo.txt ];
then
    echo "El archivo existe"
    echo
else
    echo "El archivo no existe, será creado"
    echo
    touch archivo.txt
fi


echo "Ecribir dentro de un archivo creado"
echo
echo "Este mensaje estará en el archivo creado" >> archivo.txt


exit 0
