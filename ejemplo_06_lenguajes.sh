#!/bin/bash


clear


echo
echo "Crear y ejecutar el hola mundo desde python"
cat > hola_mundo.py <<EOF
print("Hola Mundo desde Python")
EOF

python hola_mundo.py

echo
echo "Crear, compilar y ejecutar hola mundo desde C"
cat > hola_mundo.c <<EOF
#include <stdio.h>


int main(void)
{
    printf("Hola mundo desde C\n");

    return 0;
}
EOF


echo
echo "Compilando hola mundo desde C"
echo
gcc -Wall -o ejecutar_hola_mundo hola_mundo.c


echo
echo "Ejecutando hola mundo desde C"
echo
./ejecutar_hola_mundo


echo
echo "Ver los archivo creados"
ls -l *hola_mundo*


exit 0
